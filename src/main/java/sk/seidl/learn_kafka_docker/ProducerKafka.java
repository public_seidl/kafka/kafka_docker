package sk.seidl.learn_kafka_docker;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;



/**
 * @author
 *      <a href="mailto:seidl.matus@gmail.com">
 *              Matus Seidl
 *       </a>
 *
 * 2017-12-05
 */
@Component
public class ProducerKafka {

    private KafkaTemplate<String,String> kafkaTemplate;

    private Environment env;

    @Autowired
    public ProducerKafka(KafkaTemplate<String, String> kafkaTemplate, Environment env) {
        this.kafkaTemplate = kafkaTemplate;
        this.env = env;
    }

    public void sendMessage(String msg){
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(env.getProperty("kafka.topic"),msg,msg);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Inside Exception");
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                System.out.println("Inside success");
            }
        });
    }
}
