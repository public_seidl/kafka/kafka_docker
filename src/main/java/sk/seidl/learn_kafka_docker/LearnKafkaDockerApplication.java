package sk.seidl.learn_kafka_docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnKafkaDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnKafkaDockerApplication.class, args);
	}
}
